/**
 * Arduino + PHP
 *
 * @author  Bruno Soares
 * @website www.bsoares.com.br
 */

#define ANALOG_PIN 4

int val = 0;
int pinLed = 13;
int time = 100;

void setup()
{
  Serial.begin(9600);
  pinMode(pinLed, OUTPUT);
}

void loop()
{
    if (Serial.available() > 0){
      val = Serial.read();
      Serial.print((char)val, BYTE);
     
      digitalWrite(pinLed, HIGH);   // set the LED on
      //delay(time);              // wait for a second
      digitalWrite(pinLed, LOW);    // set the LED off
      //delay(time);              // wait for a second
    }
}
