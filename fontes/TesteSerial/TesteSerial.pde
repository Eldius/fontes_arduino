
int RESP_PADRAO = '+';

void setup() {
	Serial.begin(9600);
	pinMode(2, INPUT);
}

void loop() {
	if (Serial.available() > 0) {
		int readValue = digitalRead(2);
		Serial.println(readValue, DEC);
	}else{
		Serial.println(RESP_PADRAO, DEC);
	}
}

