#include <SoftSerial.h>



#define tx 11
#define rx 10

SoftSerial gps(rx,tx);
int led = 13;
int statusLed = 0;

void setup(){
    Serial.begin(9600);
    pinMode(led, OUTPUT);
}

void loop(){
    while (gps.available())
    {
        int c = gps.read();
        Serial.write(c);
        if(c == 13){
            changeLedStatus();
        }
    }
}
void changeLedStatus(){
    if(statusLed == 1){
        digitalWrite(led, HIGH);
    }else{
        digitalWrite(led, LOW);
    }
    statusLed = 1- statusLed;
}

