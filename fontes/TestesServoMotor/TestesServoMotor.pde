#include <Servo.h>

Servo servo;

int servoPin = 14;
int led = 13;

void setup() {

  pinMode(1,OUTPUT);
  pinMode(13,OUTPUT);
  servo.attach(servoPin); //analog pin 0
  //servo.setMaximumPulse(2000);
  //servo.setMinimumPulse(700);

  Serial.begin(19200);
  Serial.println("Ready");
}

void loop() {

    static int v = 0;

    if ( Serial.available()) {

      digitalWrite(led, HIGH);
      delay(500);
      digitalWrite(led, LOW);

      int pos = Serial.read();
        
        v = (pos - '0') * 10;
        
        servo.write(v);
        Serial.println(v, DEC);
    }

//  Servo::refresh();
} 
