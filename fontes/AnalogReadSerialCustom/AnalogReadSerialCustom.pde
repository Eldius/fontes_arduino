#include <SoftwareSerial.h>

/*
  AnalogReadSerial
 Reads an analog input on pin 0, prints the result to the serial monitor 
 
 This example code is in the public domain.
 */

#define rxPin2 0
#define txPin2 1


int time = 10000;
int pinLed = 13;
int baud = 9600;
SoftwareSerial mySerial2;

void setup() {
  mySerial2 =  SoftwareSerial(rxPin2, txPin2);
  mySerial2.begin(9600);
  //Serial.begin(baud);
}

void loop() {
  int sensorValue = analogRead(DEC);
  mySerial2.println(sensorValue, DEC);
}
