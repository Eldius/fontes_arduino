#include <NewSoftSerial.h>

#define ledPin 13
#define tx 3
#define rx 2
NewSoftSerial serial(tx, rx);
byte pinState = 0;

void setup()  {
  pinMode(ledPin, OUTPUT);
  
  serial.begin(9600);
  
  Serial.begin(9600);
  Serial.println("Iniciando.");
}

void loop() {
    char someChar;
    
    
    if(serial.available()){
        someChar = serial.read();
        Serial.write(someChar);
        toggle(13);
    }
}


void toggle(int pinNum) {
  // set the LED pin using the pinState variable:
  digitalWrite(pinNum, pinState); 
  // if pinState = 0, set it to 1, and vice versa:
  pinState = !pinState;
}
