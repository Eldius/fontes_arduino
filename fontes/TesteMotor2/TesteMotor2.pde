

int button = 3;
int led = 13;
int ligado = 1;
int btn = LOW;
int ctrlPin = 9;
int motorLogicPin1 = 11;
int motorLogicPin2 = 10;
int tempo = 500;
int tempoPausa;

void setup(){
    pinMode(button, INPUT);
    pinMode(led, OUTPUT);
    pinMode(ctrlPin, OUTPUT);
    pinMode(motorLogicPin1, OUTPUT);
    pinMode(motorLogicPin2, OUTPUT);
    digitalWrite(ctrlPin, HIGH);
}

void loop(){
    primeiroTesteMotor();
}

void frente(){
    digitalWrite(motorLogicPin1, LOW);   // set leg 1 of the H-bridge low
    digitalWrite(motorLogicPin2, HIGH);  // set leg 2 of the H-bridge high
}

void tras(){
    digitalWrite(motorLogicPin1, HIGH);   // set leg 1 of the H-bridge low
    digitalWrite(motorLogicPin2, LOW);  // set leg 2 of the H-bridge high
}

void primeiroTesteMotor(){
    btn = digitalRead(button);
    
    if(btn == HIGH){
        digitalWrite(motorLogicPin1, LOW);   // set leg 1 of the H-bridge low
        digitalWrite(motorLogicPin2, HIGH);  // set leg 2 of the H-bridge high
        digitalWrite(led, HIGH);  // set leg 2 of the H-bridge high
    }else{
        digitalWrite(motorLogicPin1, HIGH);   // set leg 1 of the H-bridge low
        digitalWrite(motorLogicPin2, LOW);  // set leg 2 of the H-bridge high
        digitalWrite(led, LOW);  // set leg 2 of the H-bridge high
    }
}
