
int pin = 9;
int but = 3;
int led = 9;
int ligado = 0;


void setup(){
  pinMode(led, OUTPUT);
  pinMode(but, INPUT);
}

void loop(){
    int status = digitalRead(but);
    if(status == HIGH){
        ligado = 1 - ligado;
    }
    
    if(ligado == 1){
        digitalWrite(led, HIGH);
    }else{
        digitalWrite(led, LOW);
    }
}
