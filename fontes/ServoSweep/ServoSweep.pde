#include <Servo.h> 
 
int pos = 0;

int time = 50;
int angMax = 179;
int angMin = 0;
int servoPin = 14;
Servo myservo;
int led = 13;
int passo = 20;

void setup() 
{
    myservo.attach(servoPin);  // attaches the servo on pin 9 to the servo object 
}
 
 
void loop() 
{ 
    testeIdaVolta();
/*
    myservo.write(angMin);
    delay(3000);
    analogWrite(led, 150);
    myservo.write(angMax);
    analogWrite(led, 0);
    delay(3000);
*/
} 

void testeIdaVolta(){
  for(pos = angMin; pos < angMax; pos += passo)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(time);                       // waits 15ms for the servo to reach the position 
  }
  
  
  for(pos = angMax; pos>=angMin; pos-=passo)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(time);                       // waits 15ms for the servo to reach the position 
  } 
}
