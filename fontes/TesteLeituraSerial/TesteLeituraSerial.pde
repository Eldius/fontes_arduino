int ledPin = 13; // the number of the LED pin

void setup() {
    Serial.begin(9600); // set serial speed
    pinMode(ledPin, OUTPUT); // set LED as output
    digitalWrite(ledPin, LOW); //turn off LED
}


void loop(){
    while (Serial.available() == 0); // do nothing if nothing sent    
    int idDevice = Serial.read(); // deduct ascii value of '0' to find numeric value of sent number
    while (Serial.available() == 0); // do nothing if nothing sent    
    int val = Serial.read();
    switch(idDevice){
        /*
        case 0:
            digitalWrite(ledPin, LOW); // turn off LED
            break;
        */
        case 1:
            if(val == 1){
                digitalWrite(ledPin, HIGH); // turn on LED
            }else{
                digitalWrite(ledPin, LOW); // turn on LED
            }
            break;
        default:
            break;
    }
    //Serial.println(idDevice, DEC);
    //Serial.println(val, DEC);
}

