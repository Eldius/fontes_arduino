#include <TinyGPS.h>

#include <NewSoftSerial.h>



#define RXPIN 10
#define TXPIN 11
#define LEDPIN 13

NewSoftSerial nss(RXPIN, TXPIN);
TinyGPS gps;
byte pinState = 0;

void setup(){
    pinMode(LEDPIN, OUTPUT);
    Serial.begin(9600);
}

void loop()
{
  while (nss.available())
  {
    toggle();

    int c = nss.read();
    if (gps.encode(c))
    {
        trataDados();
    }
  }
}

void trataDados(){
    float flat, flon;
    unsigned long fix_age;
    
    // returns +- latitude/longitude in degrees
    gps.f_get_position(&flat, &flon, &fix_age);
    if (fix_age == TinyGPS::GPS_INVALID_AGE)
      Serial.println("No fix detected");
    else if (fix_age > 5000)
      Serial.println("Warning: possible stale data!");
    else
      Serial.println("Data is current.");
}

void toggle() {
  // set the LED pin using the pinState variable:
  digitalWrite(LEDPIN, pinState); 
  // if pinState = 0, set it to 1, and vice versa:
  pinState = !pinState;
}
