#include <SoftwareSerial.h>

#include <TinyGPS.h>

/* This sample code demonstrates the normal use of a TinyGPS object.
   It requires the use of SoftwareSerial, and assumes that you have a
   4800-baud serial GPS device hooked up on pins 2(rx) and 3(tx).
*/
#define led 13
TinyGPS gps;
SoftwareSerial nss(2, 3);
int ledStatus = 0;
void gpsdump(TinyGPS &gps);
bool feedgps();
void printFloat(double f, int digits = 2);

void setup()
{
  Serial.begin(115200);
  nss.begin(9600);
  
  pinMode(led, OUTPUT);
  
  Serial.print("Testing");

  Serial.println();
}

void loop()
{
  while (nss.available())
  {
    if (gps.encode(nss.read()))
    {
      getPos();
      ledChange();
    }
  }
}


void getPos(){
  long lat, lon;
  float flat, flon;
  unsigned long age, date, time, chars;
  int year;
  byte month, day, hour, minute, second, hundredths;
  unsigned short sentences, failed;

  gps.get_position(&lat, &lon, &age);
  
  Serial.print("Lat: ");
  Serial.print(lat);
  Serial.print("Lon: ");
  Serial.println(lon);
}

void ledChange(){
  ledStatus = 1 - ledStatus;
  
  if(ledStatus == 1){
    digitalWrite(led, HIGH);
  }else{
    digitalWrite(led, LOW);
  }
}
