
#include <SoftwareSerial.h>

#define rxApc 3 //TXD
#define txApc 4 //RXD

#define pinLed 13
#define pinLed1 9
#define pinLed2 8

int command(char cmd);
void mudaStatusLed();
void mudaStatusLed1();
void mudaStatusLed2();

int ledStat = 0;
int ledStat1 = 0;
int ledStat2 = 0;

// Define pins for serial port emulation
SoftwareSerial apc(rxApc, txApc);

void setup(){
    // Start the emulated serial port 
    // with 9600kbps
    apc.begin(9600);
    
    Serial.begin(9600);

    pinMode(pinLed, OUTPUT);
    
    pinMode(pinLed1, OUTPUT);
    pinMode(pinLed2, OUTPUT);
    // configApc();
}

void loop(){
  
  while(apc.available()==0){}
  int cmd = apc.read();
  
  cmd = command(cmd);
  
  apc.println(cmd);
}

// Change led state (ON/OFF)
void mudaStatusLed(){
    if(ledStat == 1){
        digitalWrite(pinLed, HIGH);
        //digitalWrite(cmdLed, HIGH);
    }else{
        digitalWrite(pinLed, LOW);
        //digitalWrite(cmdLed, LOW);
    }
    ledStat = 1 - ledStat;
}

// Change led state (ON/OFF)
void mudaStatusLed1(){
    if(ledStat1 == 1){
        digitalWrite(pinLed1, HIGH);
        //digitalWrite(cmdLed, HIGH);
    }else{
        digitalWrite(pinLed1, LOW);
        //digitalWrite(cmdLed, LOW);
    }
    ledStat1 = 1 - ledStat1;
}

// Change led state (ON/OFF)
void mudaStatusLed2(){
    if(ledStat2 == 1){
        digitalWrite(pinLed2, HIGH);
        //digitalWrite(cmdLed, HIGH);
    }else{
        digitalWrite(pinLed2, LOW);
        //digitalWrite(cmdLed, LOW);
    }
    ledStat2 = 1 - ledStat2;
}

int command(char cmd){

  switch((char)cmd){
    case '0':
      mudaStatusLed();
      break;
      
    case '1':
      mudaStatusLed1();
      break;
    default:
      mudaStatusLed2();
      break;
  }
  
  return cmd;
}



