#include <SoftwareSerial.h>

#define rx 3
#define tx 4
#define pinLed 13

unsigned int ledStat = 0;

SoftwareSerial mySerial(rx, tx);

void setup(){
    Serial.begin(9600);
    mySerial.begin(9600);
    pinMode(pinLed, OUTPUT);
}

void loop(){
    if(mySerial.available()){
        Serial.print(mySerial.read());
        mudaStatusLed();
    }
}

void mudaStatusLed(){
    if(ledStat == 1){
        digitalWrite(pinLed, HIGH);
    }else{
        digitalWrite(pinLed, LOW);
    }
    ledStat = 1 - ledStat;
}

