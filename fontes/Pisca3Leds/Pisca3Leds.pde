
int ledBranco = 7;
int ledVerde = 8;
int ledVermelho = 10;

int time = 100;


void setup() {
  pinMode(ledVermelho, OUTPUT);
  pinMode(ledVerde, OUTPUT);
  pinMode(ledBranco, OUTPUT);
}

void loop() {
  digitalWrite(ledVermelho, HIGH);  // set the LED on
  delay(time);                      // wait for a second
  
  digitalWrite(ledVerde, HIGH);  // set the LED on
  delay(time);                      // wait for a second
  
  digitalWrite(ledBranco, HIGH);  // set the LED on
  delay(time);                      // wait for a second

  digitalWrite(ledVermelho, LOW);   // set the LED on
  delay(time);                      // wait for a second

  digitalWrite(ledVerde, LOW);   // set the LED on
  delay(time);                      // wait for a second

  digitalWrite(ledBranco, LOW);   // set the LED on
  delay(time);                      // wait for a second
}
