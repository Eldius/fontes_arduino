#define INFRARED 14
#define LEDPIN 13

void setup(){
    pinMode(LEDPIN, OUTPUT);
    pinMode(INFRARED, INPUT);
    Serial.begin(9600);
}

void loop(){
    int ir = analogRead(INFRARED);
    Serial.println(ir);
    delay(1000);
}
