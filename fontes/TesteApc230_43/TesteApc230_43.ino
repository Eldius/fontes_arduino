#include <SoftwareSerial.h>

#define rx 3
#define tx 4
#define set 10
#define pinLed 13

unsigned int ledStat = 0;

SoftwareSerial mySerial(rx, tx);

void setup(){
    Serial.begin(9600);
    mySerial.begin(9600);
    pinMode(pinLed, OUTPUT);
    pinMode(set, OUTPUT);
}

// WR 434000 3 9 5 1
// Padrão:
// WR 434000 3 9 3 0
// RST
void loop(){
    digitalWrite(set, LOW);
    mudaStatusLed();

    String cmd = "";

    while(Serial.available()){
        char c = Serial.read();
        cmd = cmd + c;
        delay(50);
    }

    if(cmd != ""){
        cmd = cmd + (char) 13;
        cmd = cmd + (char) 10;
      
        Serial.println("Comando: " + cmd);

        Serial.println("Parte 01");

        digitalWrite(set, HIGH);
        mudaStatusLed();
        delay(100);
        mudaStatusLed();
        Serial.println("Parte 02");
        
        digitalWrite(set, LOW);
        
        if(cmd != "RST"){
            mySerial.print(cmd);

            Serial.println("Parte 03");
            
            while(mySerial.available() == 0){}
            
            Serial.println("Parte 04");
            while(mySerial.available() != 0){
                Serial.print((char)mySerial.read());
            }

        }else{
            delay(500);
        }

        Serial.print('\n');
        digitalWrite(set, HIGH);
        mudaStatusLed();
        Serial.println("Parte 05");

        Serial.println("Fim!!\n\n");

        digitalWrite(set, LOW);
        mudaStatusLed();

    }
}

void mudaStatusLed(){
    if(ledStat == 1){
        digitalWrite(pinLed, HIGH);
    }else{
        digitalWrite(pinLed, LOW);
    }
    ledStat = 1 - ledStat;
}

