
#include <SoftwareSerial.h>

#define rxApc 3 //TXD
#define txApc 4 //RXD

#define pinLed 13
#define esq 7
#define dir 8

int command(char cmd);

// Define pins for serial port emulation
SoftwareSerial apc(rxApc, txApc);

void setup(){
    // Start the emulated serial port 
    // with 9600kbps
    apc.begin(9600);
    
    Serial.begin(9600);

    pinMode(pinLed, OUTPUT);
    pinMode(esq, OUTPUT);
    pinMode(dir, OUTPUT);
}

void loop(){
  
  if(apc.available()){
    int cmd = apc.read();
    //mudaStatusLed();
    cmd = command(cmd);
    apc.println(cmd);
  }
}

int command(char cmd){

  switch(cmd){
    case 'w':
      frente();
      break;
      
    case 'a':
      esquerda();
      break;
      
    case 'd':
      direita();
      break;
      
    case 's':
      tras();
      break;
      
    default:
      cmd = -1;
  }
  
  return cmd;
}

