
int statEsq = 0;
int statDir = 0;

void motorEsq(){
  statEsq = 1 - statEsq;
  if(statEsq == 1){
        digitalWrite(esq, HIGH);
        //digitalWrite(cmdLed, HIGH);
    }else{
        digitalWrite(esq, LOW);
        //digitalWrite(cmdLed, LOW);
    }
}

void motorDir(){
  statDir = 1 - statDir;
  if(statDir == 1){
        digitalWrite(dir, HIGH);
        //digitalWrite(cmdLed, HIGH);
    }else{
        digitalWrite(dir, LOW);
        //digitalWrite(cmdLed, LOW);
    }
}

void esquerda(){
  statDir = 1;
  statEsq = 0;
  motorEsq();
  motorDir();
}

void direita(){
  statDir = 0;
  statEsq = 1;
  motorEsq();
  motorDir();
}

void frente(){
  statDir = 0;
  statEsq = 0;
  motorEsq();
  motorDir();
}

void tras(){
  statDir = 1;
  statEsq = 1;
  motorEsq();
  motorDir();
}
