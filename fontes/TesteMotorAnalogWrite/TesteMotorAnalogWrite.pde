int button = 3;
int led = 13;
int ligado = 1;
int btn = LOW;
int ctrlPin = 9;
int motorLogicPin1 = 11;
int motorLogicPin2 = 10;
int tempo = 500;
int tempoPausa;
int cont = 0;
int mini = 110;
int maxi = 160;


void setup(){
    pinMode(button, INPUT);
    pinMode(led, OUTPUT);
    pinMode(ctrlPin, OUTPUT);
    pinMode(motorLogicPin1, OUTPUT);
    pinMode(motorLogicPin2, OUTPUT);
    digitalWrite(ctrlPin, HIGH);
    Serial.begin(9600);
    digitalWrite(ctrlPin, HIGH);
    
    digitalWrite(led, HIGH);
}

void loop(){
/*

    if(cont >= 250){
        cont = 0;
    }
  
    btn = digitalRead(button);

    if(btn == HIGH){
        frente();
    }else{
        frentePinAnalog();
    }
*/
/*
    testeMinimo();
    digitalWrite(led, HIGH);
    delay(200);
    digitalWrite(led, LOW);
    delay(5000);
*/
    testeSerial();
}

void testeSerial(){

    if(Serial.available()){
        int value = Serial.read();
        if(value != 300){
            analogWrite(motorLogicPin1, value);   // set leg 1 of the H-bridge low
            analogWrite(motorLogicPin2, LOW);  // set leg 2 of the H-bridge high
        
            Serial.println(value, DEC);
            Serial.println(HIGH, DEC);
            Serial.println(LOW, DEC);
//            Serial.println(0, DEC);
        }else{
            digitalWrite(motorLogicPin1, HIGH);   // set leg 1 of the H-bridge low
            digitalWrite(motorLogicPin2, LOW);  // set leg 2 of the H-bridge high
        }
    }
}

void frentePinAnalog(){
      analogWrite(motorLogicPin1, cont);   // set leg 1 of the H-bridge low
      analogWrite(motorLogicPin2, LOW);  // set leg 2 of the H-bridge high

      Serial.println(cont, DEC);
      
      if((cont % 50) == 0){
          digitalWrite(led, HIGH);
      }else{
          digitalWrite(led, LOW);
      }
      
      delay(300);
      
      cont += 10;
}

void testeMinimo(){
    digitalWrite(motorLogicPin1, HIGH);   // set leg 1 of the H-bridge low
    digitalWrite(motorLogicPin2, LOW);  // set leg 2 of the H-bridge high
    for(int i = mini; i <= maxi; i+=1){
        analogWrite(motorLogicPin1, i);   // set leg 1 of the H-bridge low
        analogWrite(motorLogicPin2, LOW);  // set leg 2 of the H-bridge high
        Serial.println(i, DEC);        
        delay(2000);
    }
}

void ctrlPinAnalog(){
    
      analogWrite(ctrlPin, cont);
      if((cont % 50) == 0){
          digitalWrite(led, HIGH);
      }else{
          digitalWrite(led, LOW);
      }
      delay(300);
      cont += 10;
}

void frente(){
    digitalWrite(motorLogicPin1, LOW);   // set leg 1 of the H-bridge low
    digitalWrite(motorLogicPin2, HIGH);  // set leg 2 of the H-bridge high
}
