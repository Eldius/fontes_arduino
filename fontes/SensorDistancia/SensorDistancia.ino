
/*
 * HCSR04Ultrasonic/examples/UltrasonicDemo/UltrasonicDemo.pde
 *
 * SVN Keywords
 * ----------------------------------
 * $Author: cnobile $
 * $Date: 2011-09-17 02:43:12 -0400 (Sat, 17 Sep 2011) $
 * $Revision: 29 $
 * ----------------------------------
 */

#include <SoftwareSerial.h>
#include <Ultrasonic.h>

// Sensor de distância
#define TRIGGER_PIN  12
#define ECHO_PIN     13

// Comunicação
#define apcTx  7
#define apcRx  8

// GPS
#define gpsTx  5
#define gpsRx  6

Ultrasonic ultrasonic(TRIGGER_PIN, ECHO_PIN);
SoftwareSerial apc(apcTx, apcRx);
SoftwareSerial gps(gpsTx, gpsRx);

void setup()
{
  apc.begin(9600);
  Serial.begin(9600);
}

void loop()
{
  float cmMsec, inMsec;
  long microsec = ultrasonic.timing();


  cmMsec = ultrasonic.convert(microsec, Ultrasonic::CM);
  inMsec = ultrasonic.convert(microsec, Ultrasonic::IN);
  saida("MS: ");
  saidaFloat(microsec);
  saida(", CM: ");
  saidaFloat(cmMsec);
  saida(", IN: ");
  saidaFloat(inMsec);
  saida("\n");
  delay(1000);
}

void saida(String msg){
  Serial.print(msg);
}


void saidaFloat(float msg){
  Serial.print(msg);
}

