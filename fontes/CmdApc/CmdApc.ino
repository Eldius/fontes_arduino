
#include <SoftwareSerial.h>

#define rxApc 3 //TXD
#define txApc 4 //RXD

#define pinLed 13
#define pinLed1 9
#define pinLed2 8
#define pinLanterna 10

int command(char cmd);

// Define pins for serial port emulation
SoftwareSerial apc(rxApc, txApc);

void setup(){
    // Start the emulated serial port 
    // with 9600kbps
    apc.begin(9600);
    
    Serial.begin(9600);

    pinMode(pinLed, OUTPUT);
    pinMode(pinLed1, OUTPUT);
    pinMode(pinLed2, OUTPUT);
    pinMode(pinLanterna, OUTPUT);
    // configApc();
}

void loop(){
  
  if(apc.available()){
    int cmd = apc.read();
    //mudaStatusLed();
    cmd = command(cmd);
    apc.println(cmd);
  }
}

int command(char cmd){

  switch(cmd){
    case '1':
      mudaStatusLed();
      break;
      
    case '2':
      mudaStatusLed1();
      break;
      
    case '3':
      mudaStatusLed2();
      break;
      
    case '4':
      mudaStatusLanterna();
      break;
      
    case '9':
      ligar();
      break;
      
    case '0':
      apagar();
      break;
    
    default:
      cmd = -1;
  }
  
  return cmd;
}



