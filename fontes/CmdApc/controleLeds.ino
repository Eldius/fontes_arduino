


int ledStat = 0;
int ledStat1 = 0;
int ledStat2 = 0;
int lanterna = 0;

void mudaStatusLed();
void mudaStatusLed1();
void mudaStatusLed2();
void mudaStatusLanterna();



// Change led state (ON/OFF)
void mudaStatusLed(){
    if(ledStat == 1){
        digitalWrite(pinLed, HIGH);
        //digitalWrite(cmdLed, HIGH);
    }else{
        digitalWrite(pinLed, LOW);
        //digitalWrite(cmdLed, LOW);
    }
    ledStat = 1 - ledStat;
}

// Change led state (ON/OFF)
void mudaStatusLed1(){
    if(ledStat1 == 1){
        digitalWrite(pinLed1, HIGH);
        //digitalWrite(cmdLed, HIGH);
    }else{
        digitalWrite(pinLed1, LOW);
        //digitalWrite(cmdLed, LOW);
    }
    ledStat1 = 1 - ledStat1;
}

// Change led state (ON/OFF)
void mudaStatusLed2(){
    if(ledStat2 == 1){
        digitalWrite(pinLed2, HIGH);
        //digitalWrite(cmdLed, HIGH);
    }else{
        digitalWrite(pinLed2, LOW);
        //digitalWrite(cmdLed, LOW);
    }
    ledStat2 = 1 - ledStat2;
}

void apagar(){

    ledStat = 0;
    ledStat1 = 0;
    ledStat2 = 0;
    lanterna = 0;

    mudaStatusLed();
    mudaStatusLed1();
    mudaStatusLed2();
    mudaStatusLanterna();
}

void ligar(){

    ledStat = 1;
    ledStat1 = 1;
    ledStat2 = 1;
    lanterna = 1;

    mudaStatusLed();
    mudaStatusLed1();
    mudaStatusLed2();
    mudaStatusLanterna();
}


void mudaStatusLanterna(){
    if(lanterna == 1){
        digitalWrite(pinLanterna, HIGH);
        //digitalWrite(cmdLed, HIGH);
    }else{
        digitalWrite(pinLanterna, LOW);
        //digitalWrite(cmdLed, LOW);
    }
    lanterna = 1 - lanterna;
}
