#include <SoftwareSerial.h>

#define rx 3 //TXD
#define tx 4 //RXD
#define pinLed 13

unsigned int ledStat = 0;

// Define pins for serial port emulation
SoftwareSerial mySerial(rx, tx);

void setup(){
    // Start the emulated serial port 
    // with 9600kbps
    mySerial.begin(9600);
    pinMode(pinLed, OUTPUT);
}

void loop(){
    char msg;
    if(mySerial.available()){
        // Read data from soft serial
        msg = mySerial.read();
        // Read data from soft serial
        mySerial.print(msg);
        mudaStatusLed();
    }
}

// Change led state (ON/OFF)
void mudaStatusLed(){
    if(ledStat == 1){
        digitalWrite(pinLed, HIGH);
    }else{
        digitalWrite(pinLed, LOW);
    }
    ledStat = 1 - ledStat;
}

