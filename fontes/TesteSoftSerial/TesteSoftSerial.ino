#include <SoftwareSerial.h>

SoftwareSerial mySerial(10, 11);
int led = 13;
int statusLed = 0;

void setup()  
{
  Serial.begin(9600);
  Serial.println("Goodnight moon!");

  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  pinMode(led, OUTPUT);
}

void loop() // run over and over
{
    while(mySerial.available()==0);
    char c = mySerial.read();
    Serial.write(c);
  
    if(c == 13){
        changeLedStatus();
    }
}

void changeLedStatus(){
    if(statusLed == 1){
        digitalWrite(led, HIGH);
    }else{
        digitalWrite(led, LOW);
    }
    statusLed = 1- statusLed;
}

