int button = 3;
int led = 13;
int ligado = 1;
int btn = LOW;
int ctrlPin = 9;
int motorLogicPin1 = 11;
int motorLogicPin2 = 10;
int tempo = 500;
int tempoPausa;
int cont = 0;
int mini = 110;
int maxi = 160;


void setup(){
    pinMode(led, OUTPUT);
    pinMode(ctrlPin, OUTPUT);
    pinMode(motorLogicPin1, OUTPUT);
    pinMode(motorLogicPin2, OUTPUT);
    digitalWrite(ctrlPin, HIGH);
    Serial.begin(9600);
}

void loop(){
    testeSerial();
}

void testeSerial(){
    int value;
    int idDevice;

    if(Serial.available()){
        idDevice = Serial.read();
        while(Serial.available()==0)
        value = Serial.read();
        switch(idDevice){
            case 0: //motor 01
                value = (value - 50) * 2;
                if(value > 0){
                    frente((value/100) * 255);
                }else if(value < 0){
                    tras(((0 - value)/100) * 255);
                }else{
                    parar();
                }
                break;
            
            case 1: //LED
                if(value == 1){
                    digitalWrite(led, HIGH);
                }else{
                    digitalWrite(led, LOW);
                }
            case 2: //Motor 01 velocidade total
                if(value == 1){
                    frente1();
                }else{
                    tras1();
                }
            default:
                break;
        }
    }
}


void frente(int pot){
    digitalWrite(motorLogicPin1, LOW);   // set leg 1 of the H-bridge low
    analogWrite(motorLogicPin2, pot);  // set leg 2 of the H-bridge high
}

void tras(int pot){
    analogWrite(motorLogicPin1, pot);   // set leg 1 of the H-bridge low
    digitalWrite(motorLogicPin2, LOW);  // set leg 2 of the H-bridge high
}

void frente1(){
    digitalWrite(motorLogicPin1, LOW);   // set leg 1 of the H-bridge low
    digitalWrite(motorLogicPin2, HIGH);  // set leg 2 of the H-bridge high
}

void tras1(){
    digitalWrite(motorLogicPin1, HIGH);   // set leg 1 of the H-bridge low
    digitalWrite(motorLogicPin2, LOW);  // set leg 2 of the H-bridge high
}

void parar(){
    digitalWrite(motorLogicPin1, LOW);   // set leg 1 of the H-bridge low
    digitalWrite(motorLogicPin2, LOW);  // set leg 2 of the H-bridge high
}

