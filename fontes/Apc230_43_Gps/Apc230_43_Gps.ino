
#include <SoftwareSerial.h>

#define rxApc 3 //TXD
#define txApc 4 //RXD

#define rxGps 7 //TXD
#define txGps 6 //RXD

#define pinLed 13

unsigned int ledStat = 0;

// Define pins for serial port emulation
SoftwareSerial apc(rxApc, txApc);
SoftwareSerial gps(rxGps, txGps);

String nmea;


void setup(){
    // Start the emulated serial port 
    // with 9600kbps
    apc.begin(9600);
    
    Serial.begin(9600);
    gps.begin(9600);

    pinMode(pinLed, OUTPUT);
    // configApc();    
}

void loop(){
    String msg;
    int count;
    while(gps.available()){

        // Read data from soft serial
        char c = gps.read();
        
        msg = msg + c;
        
        if((int)c == 10){
          count++;
          if(count==2){
            Serial.print(msg);
            apc.print(msg);
            msg = "";
            mudaStatusLed();
            nmea = msg;
            count = 0;
          }
        }
    }
}

// Change led state (ON/OFF)
void mudaStatusLed(){
    if(ledStat == 1){
        digitalWrite(pinLed, HIGH);
    }else{
        digitalWrite(pinLed, LOW);
    }
    ledStat = 1 - ledStat;
}
/*
void configApc(){
    
    digitalWrite(pinLed, HIGH);

    Serial.println("Parte 01");

    int setApc = 12; //SET
    // WR_FREQ_REF-DATA-RATE_OUT-POWER_SERIES-DATA-RATE_CHECKOUT
    // 434MHz, 9600bps, 20mW, 19200bps, express even parity
    // String strConfig = "WR_434000_3_9_5_1\n";
    String strConfig = "RD";
    int tam = 17;

    Serial.println("Parte 02");

    pinMode(setApc, OUTPUT);
    
    digitalWrite(setApc, HIGH);
    delay(100);
    digitalWrite(setApc, LOW);
    // delay(2);
    apc.print(strConfig);
    // apc.print((char)13);
    // apc.print((char)10);

    Serial.println("Parte 03");

    Serial.print(strConfig);
    Serial.print((char)13);
    Serial.print((char)10);


    Serial.println("Parte 04");

    digitalWrite(setApc, HIGH);
    
    while(apc.available() == 0){}

    Serial.println("Parte 05");
    
    while(apc.available() != 0){
        Serial.print(apc.read());
    }

    Serial.println("Parte 06");
    
    digitalWrite(pinLed, HIGH);
    
    delay(500);

    Serial.println("Parte 07");
    
    digitalWrite(setApc, LOW);
}
*/


