#include <SoftwareSerial.h>

/*
  AnalogReadSerial
 Reads an analog input on pin 0, prints the result to the serial monitor 
 
 This example code is in the public domain.
 */

#define rxPin1 0
#define txPin1 1
#define rxPin2 9
#define txPin2 10

byte pinState = 0;
int time = 10000;
int pinLed = 13;
int baud = 9600;
SoftwareSerial mySerial1 = SoftwareSerial(rxPin1, txPin1);
SoftwareSerial mySerial2 = SoftwareSerial(rxPin2, txPin2);

void setup() {
  //mySerial1.begin(9600);
  mySerial2.begin(9600);
  Serial.begin(baud);
}

void loop() {
  char c = 'P';
  char pos = mySerial2.read();
  Serial.println(pos, DEC);
  toggle(13);
  delay(500);
}

void toggle(int pinNum) {
  pinMode(pinNum, OUTPUT);
  // set the LED pin using the pinState variable:
  digitalWrite(pinNum, pinState); 
  // if pinState = 0, set it to 1, and vice versa:
  pinState = !pinState;
}

