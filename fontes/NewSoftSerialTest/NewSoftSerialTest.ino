
#include <NewSoftSerial.h>
int led = 13;
int statusLed = 0;
NewSoftSerial mySerial(10, 11);

void setup()  
{
  Serial.begin(9600);
  Serial.println("Goodnight moon!");

  // set the data rate for the NewSoftSerial port
  mySerial.begin(9600);
}

void loop()                     // run over and over again
{
  while(mySerial.available()==0);
  int c = mySerial.read();
  Serial.print((char)c);
  if(c == 13){
      changeLedStatus();
  }
}

void changeLedStatus(){
    if(statusLed == 1){
        digitalWrite(led, HIGH);
    }else{
        digitalWrite(led, LOW);
    }
    statusLed = 1- statusLed;
}
